using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using odrs.Dtos.Document;
using odrs.Dtos.Students;
using odrs.Dtos.Transaction;
using odrs.Dtos.Users;
using odrs.Models;

namespace odrs
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            // CreateMap<User, UserRegisterDto>();
            CreateMap<Student, GetStudentDto>();
            CreateMap<AddStudentDto, Student>();

            CreateMap<Transaction, GetTransactionDto>();
            CreateMap<AddTransactionDto, Transaction>();

            CreateMap<Document, GetDocumentDto>();
            CreateMap<GetDocumentDto, Document>();
            CreateMap<UpdateDocumentDto, Document>();
        }
    }
}