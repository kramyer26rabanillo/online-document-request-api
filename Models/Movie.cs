﻿using System;
using System.Collections.Generic;

namespace odrs.Models
{
    public partial class Movie
    {
        public string Title { get; set; }
        public string DateReleased { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
    }
}
