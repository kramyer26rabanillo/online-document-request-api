﻿using System;
using System.Collections.Generic;

namespace odrs.Models
{
    public partial class Transaction
    {
        public int TransactionId { get; set; }
        public string StudentNumber { get; set; }
        public int? UserId { get; set; }
        public int? DocumentId { get; set; }
        public string DateRequested { get; set; }
        public string DateReleased { get; set; }
        public string Status { get; set; }

        public virtual Document Document { get; set; }
        public virtual Student StudentNumberNavigation { get; set; }
        public virtual User User { get; set; }
    }
}
