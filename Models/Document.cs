﻿using System;
using System.Collections.Generic;

namespace odrs.Models
{
    public partial class Document
    {
        public Document()
        {
            Transactions = new HashSet<Transaction>();
        }

        public int DocumentId { get; set; }
        public string DocuDescription { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
