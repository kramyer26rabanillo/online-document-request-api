﻿using System;
using System.Collections.Generic;

namespace odrs.Models
{
    public partial class Student
    {
        public Student()
        {
            Transactions = new HashSet<Transaction>();
        }

        public string StudentNumber { get; set; }
        public string StudentName { get; set; }
        public string MobileNo { get; set; }
        public string EmailAddress { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string AdmissionYear { get; set; }
        public string Course { get; set; }
        public int Section { get; set; }
        public string YearLevel { get; set; }
        public string EnrollmentStatus { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
