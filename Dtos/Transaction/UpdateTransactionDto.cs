using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace odrs.Dtos.Transaction
{
    public class UpdateTransactionDto
    {
        public int TransactionId { get; set; }
        public string Status { get; set; }
    }
}