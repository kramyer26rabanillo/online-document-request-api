using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace odrs.Dtos.Transaction
{
    public class GetTransactionDto
    {
        public int TransactionId { get; set; }
        public string StudentNumber { get; set; }
        public int? UserId { get; set; }
        public int? DocumentId { get; set; }
        public string DateRequested { get; set; }
        public string DateReleased { get; set; }
        public string Status { get; set; }
    }
}