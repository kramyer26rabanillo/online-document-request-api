using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace odrs.Dtos.Document
{
    public class GetDocumentDto
    {
        public string DocuDescription { get; set; }
    }
}