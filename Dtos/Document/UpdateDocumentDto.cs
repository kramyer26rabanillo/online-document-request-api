using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace odrs.Dtos.Document
{
    public class UpdateDocumentDto
    {
        public int DocumentId { get; set; }
        public string DocuDescription { get; set; }
    }
}