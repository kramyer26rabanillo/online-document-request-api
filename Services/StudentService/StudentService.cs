using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using odrs.Dtos.Students;
using odrs.Models;

namespace odrs.Services.StudentService
{
    public class StudentService : IStudentService
    {
        private readonly odr_DbContext _context;
        private readonly IMapper _mapper;

        public StudentService(odr_DbContext context, IMapper mapper)
        { 
            _context = context;
            _mapper = mapper;
        }
        public async Task<ServiceResponse<List<GetStudentDto>>> AddStudent(AddStudentDto newStudent)
        {
            ServiceResponse<List<GetStudentDto>> serviceResponse = new ServiceResponse<List<GetStudentDto>>();
            if(await _context.Students.AnyAsync(x => x.EmailAddress == newStudent.EmailAddress ))
            {
                serviceResponse.Success = false;
                serviceResponse.Message = "Email Already Exist";
                return serviceResponse;
            }
            try{
                Student addStudent = _mapper.Map<Student>(newStudent);
                await _context.Students.AddAsync(addStudent);
                await _context.SaveChangesAsync();
                serviceResponse.Data = (_context.Students.Select(c=> _mapper.Map<GetStudentDto>(c)).ToList());
            }   catch(Exception ex){
                serviceResponse.Success = false;
                serviceResponse.Message = ex.Message;
            }
            
            return serviceResponse;
        }

        public async Task<ServiceResponse<List<GetStudentDto>>> DeleteStudent(string sn)
        {
            ServiceResponse<List<GetStudentDto>> serviceResponse = new ServiceResponse<List<GetStudentDto>>();
            try{
            Student student = await _context.Students.FirstAsync(c => c.StudentNumber == sn);
            _context.Students.Remove(student);
            await _context.SaveChangesAsync();
            
            List<Student> students = await _context.Students.ToListAsync();
            serviceResponse.Data = (students.Select(c => _mapper.Map<GetStudentDto>(c))).ToList();
            } catch(Exception ex) {
                serviceResponse.Success = false;
                serviceResponse.Message = ex.Message;
            }
            return serviceResponse;
        }

        public async Task<ServiceResponse<List<GetStudentDto>>> GetAllStudents()
        {
            ServiceResponse<List<GetStudentDto>> serviceResponse = new ServiceResponse<List<GetStudentDto>>();
            List<Student> students = await _context.Students.ToListAsync();
            serviceResponse.Data = (students.Select(c => _mapper.Map<GetStudentDto>(c))).ToList();
            return serviceResponse;

        }

        public async Task<ServiceResponse<GetStudentDto>> GetStudentById(string sn)
        {
            ServiceResponse<GetStudentDto> serviceResponse = new ServiceResponse<GetStudentDto>();
            Student student = await _context.Students.FirstOrDefaultAsync(c => c.StudentNumber == sn);
            serviceResponse.Data = _mapper.Map<GetStudentDto>(student);
            return serviceResponse;
        }

        public async Task<ServiceResponse<GetStudentDto>> UpdateStudent(AddStudentDto updatedStudent)
        {
            ServiceResponse<GetStudentDto> serviceResponse = new ServiceResponse<GetStudentDto>();
            try{
            Student student = await _context.Students.FirstOrDefaultAsync(c => c.StudentNumber == updatedStudent.StudentNumber);
            student.StudentNumber = updatedStudent.StudentNumber;
            student.StudentName = updatedStudent.StudentName;
            student.MobileNo = updatedStudent.MobileNo;
            student.BirthDate = updatedStudent.BirthDate;
            student.Gender = updatedStudent.Gender;
            student.Address = updatedStudent.Address;
            student.AdmissionYear = updatedStudent.AdmissionYear;
            student.Course = updatedStudent.Course;
            student.Section = updatedStudent.Section;
            student.YearLevel = updatedStudent.YearLevel;
            student.EnrollmentStatus = updatedStudent.EnrollmentStatus;
            student.EmailAddress= updatedStudent.EmailAddress;

            _context.Students.Update(student);
            await _context.SaveChangesAsync();

            serviceResponse.Data = _mapper.Map<GetStudentDto>(student);

            } catch(Exception ex) {
                serviceResponse.Success = false;
                serviceResponse.Message = ex.Message;
            }
            return serviceResponse;
        }
    }
}