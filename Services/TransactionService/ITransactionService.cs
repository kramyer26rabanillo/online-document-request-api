using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using odrs.Dtos.Transaction;
using odrs.Models;

namespace odrs.Services.TransactionService
{
    public interface ITransactionService
    {
        Task<ServiceResponse<List<GetTransactionDto>>> GetAllTransaction();
        Task<ServiceResponse<GetTransactionDto>> GetTransactionById(int id);
        Task<ServiceResponse<List<GetTransactionDto>>> AddTransaction(AddTransactionDto newTransactionDto);
        Task<ServiceResponse<GetTransactionDto>> UpdateTransaction(UpdateTransactionDto updatedTransaction);
        Task<ServiceResponse<List<GetTransactionDto>>> DeleteTransaction(int id);

    }
}