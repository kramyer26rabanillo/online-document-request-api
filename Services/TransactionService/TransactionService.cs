using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using odrs.Dtos.Transaction;
using odrs.Models;

namespace odrs.Services.TransactionService
{
    public class TransactionService : ITransactionService
    {
        private readonly odr_DbContext _context;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAcessor;

        public TransactionService(odr_DbContext context, IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _mapper = mapper;
            _httpContextAcessor = httpContextAccessor;
        }
        private int GetUserId() => int.Parse(_httpContextAcessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
        private string GetUserRole() => _httpContextAcessor.HttpContext.User.FindFirstValue(ClaimTypes.Role);
        private string GetUserEmail() => _httpContextAcessor.HttpContext.User.FindFirstValue(ClaimTypes.Name);

        public async Task<ServiceResponse<List<GetTransactionDto>>> AddTransaction(AddTransactionDto newTransactionDto)
        {
            ServiceResponse<List<GetTransactionDto>> response = new ServiceResponse<List<GetTransactionDto>>();
            Document document = await _context.Documents.FirstAsync(d => d.DocuDescription == newTransactionDto.description);
            if (!await _context.Documents.AnyAsync(d => d.DocuDescription == newTransactionDto.description))
            {
                response.Data = _context.Transactions.Select(c => _mapper.Map<GetTransactionDto>(c)).ToList();
                response.Success = false;
                response.Message = "Document type does not exist";
                return response;

            }
            Student student = await _context.Students.FirstAsync(c => c.EmailAddress == GetUserEmail());
            if (await _context.Transactions.AnyAsync(t => t.StudentNumber == student.StudentNumber && t.DocumentId == document.DocumentId))
            {
                response.Data = (_context.Transactions.Select(c => _mapper.Map<GetTransactionDto>(c))).ToList();
                response.Success = false;
                response.Message = "Request to same document already exist";
                return response;
            }
            Transaction transaction = _mapper.Map<Transaction>(newTransactionDto);
            transaction.StudentNumber = student.StudentNumber;
            transaction.DateRequested = DateTime.Today.ToString("dd-MM-yyyy");
            transaction.DocumentId = document.DocumentId;
            transaction.Status = "Pending";
            await _context.Transactions.AddAsync(transaction);
            await _context.SaveChangesAsync();

            response.Data = (_context.Transactions.Select(c => _mapper.Map<GetTransactionDto>(c))).ToList();
            return response;


        }

        public async Task<ServiceResponse<List<GetTransactionDto>>> GetAllTransaction()
        {
            ServiceResponse<List<GetTransactionDto>> response = new ServiceResponse<List<GetTransactionDto>>();
            List<Transaction> transactions;
            if(GetUserRole() != "Student")
            {
                transactions = await _context.Transactions.ToListAsync();
                response.Data =  (transactions.Select(c => _mapper.Map<GetTransactionDto>(c))).ToList();
                return response;
            }
            
            transactions = await _context.Transactions.Where(c => c.UserId == GetUserId()).ToListAsync();
            response.Data =  (transactions.Select(c => _mapper.Map<GetTransactionDto>(c))).ToList();
            return response;
            
            
        }

        public async Task<ServiceResponse<GetTransactionDto>> GetTransactionById(int id)
        {
            ServiceResponse<GetTransactionDto> response = new ServiceResponse<GetTransactionDto>();
            Transaction transaction;
            if(GetUserRole() != "Student")
            {
                transaction = await _context.Transactions.FirstOrDefaultAsync(c => c.TransactionId == id);
                response.Data = _mapper.Map<GetTransactionDto>(transaction);
                return response;
            }
            User user =await _context.Users.FirstOrDefaultAsync(c => c.UserId == GetUserId());
            Student student = await _context.Students.FirstOrDefaultAsync(c => c.EmailAddress == user.Email);
            transaction = await _context.Transactions.FirstOrDefaultAsync(c => c.TransactionId == id && c.StudentNumber == student.StudentNumber);
            response.Data = _mapper.Map<GetTransactionDto>(transaction);
            return response;


        }

        public async Task<ServiceResponse<GetTransactionDto>> UpdateTransaction(UpdateTransactionDto updatedTransaction)
        {
            ServiceResponse<GetTransactionDto> response = new ServiceResponse<GetTransactionDto>();
            if(updatedTransaction.Status != "Pending" && updatedTransaction.Status != "On Process" && updatedTransaction.Status != "Ready" && updatedTransaction.Status != "Denied" && updatedTransaction.Status != "Completed")
            {
                response.Success = false;
                response.Message = "Invalid Status";
                return response;
            }
            try{
            Transaction transaction = await _context.Transactions.FirstAsync(c => c.TransactionId == updatedTransaction.TransactionId);
            transaction.Status = updatedTransaction.Status;
            _context.Transactions.Update(transaction);
            await _context.SaveChangesAsync();

            response.Data = _mapper.Map<GetTransactionDto>(transaction);
            } catch(Exception ex) {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }

        public async Task<ServiceResponse<List<GetTransactionDto>>> DeleteTransaction(int id)
        {
            ServiceResponse<List<GetTransactionDto>> response = new ServiceResponse<List<GetTransactionDto>>();
            try
            {
                Transaction transaction = await _context.Transactions.FirstAsync(c => c.TransactionId == id);
                _context.Transactions.Remove(transaction);
                await _context.SaveChangesAsync();

                response.Data =await _context.Transactions.Select(c => _mapper.Map<GetTransactionDto>(c)).ToListAsync();
            }
            catch(Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
            
        }
    }
}