using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using odrs.Dtos.Document;
using odrs.Models;

namespace odrs.Services.DocumentService
{
    public interface IDocumentService
    {
        Task<ServiceResponse<List<GetDocumentDto>>> GetAllDocument();
        Task<ServiceResponse<GetDocumentDto>> GetDocumentById(int id);
        Task<ServiceResponse<List<GetDocumentDto>>> AddDocument(GetDocumentDto newDocument);
        Task<ServiceResponse<GetDocumentDto>> UpdateDocument(UpdateDocumentDto updatedDocument);
        Task<ServiceResponse<List<GetDocumentDto>>> DeleteDocument(int id);
    }
}