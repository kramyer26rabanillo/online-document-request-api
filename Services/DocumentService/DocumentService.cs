using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using odrs.Dtos.Document;
using odrs.Models;

namespace odrs.Services.DocumentService
{
    public class DocumentService : IDocumentService
    {
        private readonly odr_DbContext _context;
        private readonly IMapper _mapper;

        public DocumentService(odr_DbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<ServiceResponse<List<GetDocumentDto>>> AddDocument(GetDocumentDto newDocument)
        {
            ServiceResponse<List<GetDocumentDto>> response = new ServiceResponse<List<GetDocumentDto>>();
            if(await _context.Documents.AnyAsync(c=> c.DocuDescription == newDocument.DocuDescription))
            {
                response.Success = false;
                response.Message = "Document already exist";
                return response;
            }
            try
            {
            Document document = _mapper.Map<Document>(newDocument);
            await _context.Documents.AddAsync(document);
            await _context.SaveChangesAsync();
            response.Data = await _context.Documents.Select(c => _mapper.Map<GetDocumentDto>(c)).ToListAsync();
            }
            catch(Exception ex)
            {
                response.Success = false;
                response.Message =ex.Message;
            }
            return response;
        }

        public async Task<ServiceResponse<List<GetDocumentDto>>> DeleteDocument(int id)
        {
            ServiceResponse<List<GetDocumentDto>> response = new ServiceResponse<List<GetDocumentDto>>();
            try
            {
                Document document = await _context.Documents.FirstAsync(c => c.DocumentId == id);
                _context.Documents.Remove(document);
                await _context.SaveChangesAsync();

                response.Data =await _context.Documents.Select(c => _mapper.Map<GetDocumentDto>(c)).ToListAsync();
            }
            catch(Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            
            return response;
        }

        public async Task<ServiceResponse<List<GetDocumentDto>>> GetAllDocument()
        {
            ServiceResponse<List<GetDocumentDto>> response = new ServiceResponse<List<GetDocumentDto>>();
            List<Document> documents = await _context.Documents.ToListAsync();
            response.Data = await _context.Documents.Select(c => _mapper.Map<GetDocumentDto>(c)).ToListAsync();
            return response;
        }

        public async Task<ServiceResponse<GetDocumentDto>> GetDocumentById(int id)
        {
            ServiceResponse<GetDocumentDto> response = new ServiceResponse<GetDocumentDto>();
            Document document = await _context.Documents.FirstAsync(c => c.DocumentId == id);
            response.Data = _mapper.Map<GetDocumentDto>(document);
            return response;

        }

        public async Task<ServiceResponse<GetDocumentDto>> UpdateDocument(UpdateDocumentDto updatedDocument)
        {
            ServiceResponse<GetDocumentDto> response = new ServiceResponse<GetDocumentDto>();
            try
            {
                Document document = await _context.Documents.FirstAsync(c => c.DocumentId == updatedDocument.DocumentId);
                document.DocuDescription = updatedDocument.DocuDescription;
                _context.Documents.Update(document);
                await _context.SaveChangesAsync();
                response.Data = _mapper.Map<GetDocumentDto>(document);
            }
            catch(Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}