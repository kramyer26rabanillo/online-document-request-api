using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using odrs.Dtos.Transaction;
using odrs.Services.TransactionService;

namespace odrs.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class TransactionController : ControllerBase
    {
        private readonly ITransactionService _transactionService;

        public TransactionController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }
        [Authorize(Roles = "Administrator, Faculty")]
        [HttpGet("Getall")]
        public async Task<IActionResult> GetAllTransaction()
        {
            return Ok(await _transactionService.GetAllTransaction());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTransactionById(int id)
        {
            return Ok(await _transactionService.GetTransactionById(id));
        }
        [Authorize(Roles = "Student")]
        [HttpPost]
        public async Task<IActionResult> AddTransaction(AddTransactionDto newTransactionDto)
        {
            return Ok(await _transactionService.AddTransaction(newTransactionDto));
        }
        [Authorize(Roles = "Administrator, Faculty")]
        [HttpPut]
        public async Task<IActionResult> UpdateTransaction(UpdateTransactionDto updateTransactionDto)
        {
            return Ok(await _transactionService.UpdateTransaction(updateTransactionDto));
        }
        [Authorize(Roles = "Administrator, Student")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTransaction(int id)
        {
            return Ok(await _transactionService.DeleteTransaction(id));
        }
    }
}