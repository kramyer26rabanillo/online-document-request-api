using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using odrs.Dtos.Document;
using odrs.Services.DocumentService;

namespace odrs.Controllers
{
    [Authorize(Roles ="Administrator")]
    [ApiController]
    [Route("[controller]")]
    public class DocumentController : ControllerBase
    {
        private readonly IDocumentService _documentService;

        public DocumentController(IDocumentService documentService)
        {
            _documentService = documentService;
        }
        [HttpGet("Getall")]
        public async Task<IActionResult> GetAllDocument()
        {
            return Ok(await _documentService.GetAllDocument());
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDocumentById(int id)
        {
            return Ok(await _documentService.GetDocumentById(id));
        }
        [HttpPost]
        public async Task<IActionResult> AddDocument(GetDocumentDto newDocument)
        {
            return Ok(await _documentService.AddDocument(newDocument));
        }
        [HttpPut]
        public async Task<IActionResult> UpdateDocument(UpdateDocumentDto updatedDocument)
        {
            return Ok(await _documentService.UpdateDocument(updatedDocument));
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDocument(int id)
        {
            return Ok(await _documentService.DeleteDocument(id));
        }
    }
}