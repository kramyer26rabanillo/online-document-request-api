using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using odrs.Dtos.Students;
using odrs.Models;
using odrs.Services.StudentService;

namespace odrs.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class studentController : ControllerBase
    {
        private readonly IStudentService _studentService;

        public studentController(IStudentService studentService)
        {
            _studentService = studentService;
        }
        [Authorize(Roles = "Administrator, Faculty")]
        [HttpGet("Getall")]
        public async Task<IActionResult> Get()
        {
            return Ok(await _studentService.GetAllStudents());
        }
        
        [HttpGet("{sn}")]
        public async Task<IActionResult> GetById(string sn)
        {
            return Ok(await _studentService.GetStudentById(sn));
        }
        [Authorize(Roles = "Administrator,Faculty")]
        [HttpPost]
        public async Task<IActionResult> AddStudent(AddStudentDto newStudent)
        {
            return Ok(await _studentService.AddStudent(newStudent));
        }
        [HttpPut]
        public async Task<IActionResult> UpdateStudent(AddStudentDto updatedStudent)
        {
            return Ok(await _studentService.UpdateStudent(updatedStudent));
        }
        [Authorize(Roles = "Administrator,Faculty")]
        [HttpDelete("{sn}")]
        public async Task<IActionResult> DeleteStudent(string sn)
        {
            return Ok(await _studentService.DeleteStudent(sn));
        }
    }
}